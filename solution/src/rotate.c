//
// Created by natan on 02.01.2022.
//

#include "../include/rotate.h"

struct image rotate( struct image const source ) {
    struct image result = image_create();
    image_data_full(&result, source.height, source.width);

    for(size_t i = 0; i < result.height; i++) {
        for(size_t j = 0; j < result.width; j++) {
            struct maybe_pixel p = image_pixel_get(&source, source.height - 1 - j, i);
            image_pixel_set(&result, i, j, p.value); // пиксель всегда валидный, можно сразу брать значение. Было нужно для отладки функци
        }
    }

    return result;
}

