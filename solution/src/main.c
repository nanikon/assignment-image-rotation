#include "../include/bmp.h"
#include "../include/file_manager.h"
#include "../include/image.h"
#include "../include/messages.h"
#include "../include/rotate.h"
#include "../include/util.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3) { LOG_AND_STOP_IF_NOT_OK(get_incorrect_args_message(), 0, 1); }
    char* source_file_name = argv[1];
    char* target_file_name = argv[2];
    FILE* source_file = NULL;
    FILE* target_file = NULL;
    struct image source_image = image_create();

    enum open_status open_source_result = file_open_to_read(&source_file, source_file_name);
    LOG_AND_STOP_IF_NOT_OK(get_open_file_message(open_source_result), OPEN_READ_OK, open_source_result);

    enum read_status read_result = from_bmp(source_file, &source_image);
    LOG_AND_STOP_IF_NOT_OK(get_read_file_message(read_result), READ_OK, read_result);

    enum close_status close_source_result = file_close(source_file);
    LOG_AND_STOP_IF_NOT_OK(get_close_file_message(close_source_result), CLOSE_OK, close_source_result);

    struct image target_image = rotate(source_image);

    enum open_status open_target_result = file_open_to_write(&target_file, target_file_name);
    LOG_AND_STOP_IF_NOT_OK(get_open_file_message(open_target_result), OPEN_WRITE_OK, open_target_result);

    enum write_status write_result = to_bmp(target_file, &target_image);
    LOG_AND_STOP_IF_NOT_OK(get_write_file_message(write_result), WRITE_OK, write_result);

    image_destroy(&source_image);
    image_destroy(&target_image);

    enum close_status close_target_result = file_close(target_file);
    LOG_AND_STOP_IF_NOT_OK(get_close_file_message(close_target_result), CLOSE_OK, close_target_result);
    return 0;
}
