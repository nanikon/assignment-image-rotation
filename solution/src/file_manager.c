//
// Created by natan on 04.01.2022.
//

#include "../include/file_manager.h"

// TODO: подумать над разграничением разных ошибок (через errno?)
enum open_status file_open_to_read(FILE** file, const char* file_name) {
    *file = fopen(file_name, "rb");
    if (*file) { return OPEN_READ_OK; }
    else { return OPEN_READ_ERROR; }
}

enum open_status file_open_to_write(FILE** file, const char* file_name) {
    *file = fopen(file_name, "wb");
    if (*file) { return OPEN_WRITE_OK; }
    else { return OPEN_WRITE_ERROR; }
}

enum close_status file_close(FILE* file) {
    int result = fclose(file);
    if (result != 0) { return CLOSE_ERROR; }
    else { return CLOSE_OK; }
}
