//
// Created by natan on 27.12.2021.
//

#include "../include/image.h"

const struct maybe_pixel none_pixel = (struct maybe_pixel){0};

const struct maybe_image_row none_row = (struct maybe_image_row){0};

static struct maybe_pixel some_pixel(struct pixel value) {
    return (struct maybe_pixel) { .valid = true, .value = value };
}

static struct maybe_image_row some_image_row(struct pixel* value) {
    return (struct maybe_image_row) { .valid = true, .value = value };
}

struct image image_create() {
    return (struct image) {0};
}

void image_data_full(struct image* image, size_t width, size_t height) {
    image->data = malloc(sizeof(struct pixel) * width * height);
    image->width = width;
    image->height = height;
}

bool image_pixel_set(struct image* image, size_t i, size_t j, struct pixel value) {
    if ((j >= image->width) || (i >= image->height)) { return false; }
    image->data[ image->width * i + j ] = value;
    return true;
}

struct maybe_pixel image_pixel_get(const struct image* image, size_t i, size_t j) {
    if ((j >= image->width) || (i >= image->height)) { return none_pixel; }
    return some_pixel(image->data[ image->width * i + j ]);
}

struct maybe_image_row image_row_get(const struct image* image, size_t i) {
    if (i >= image->height) { return none_row; }
    return some_image_row(image->data + image->width * i);
}

void image_destroy(struct image* image) {
    free(image->data);
}
