//
// Created by natan on 09.01.2022.
//

#include "bmp.h"
#include "file_manager.h"

// больше восклицательных знаков богу восклицательных знаков
const char* incorrect_args = "Error! Expect 2 arguments!";

const char* const open_file_messages[] = {
        [OPEN_READ_OK] = "Successful open file to read!",
        [OPEN_WRITE_OK] = "Successful open file to write!",
        [OPEN_READ_ERROR] = "Can't open file to read!",
        [OPEN_WRITE_ERROR] = "Can't open file to write!"
};

const char* const close_file_messages[] = {
        [CLOSE_OK] = "Successful close file!",
        [CLOSE_ERROR] ="Can't close file!"
};

const char* const read_file_messages[] = {
        [READ_OK] = "Read successful!",
        [READ_INVALID_SIGNATURE] = "Wrong signature in header!",
        [READ_INVALID_HEADER] = "Wrong header!",
        [READ_INVALID_BODY] = "Wrong body!",
        [READ_INVALID_OFFSET] = "Wrong offset in header!",
        [READ_INVALID_PADDING] = "Can't seek padding!"
};

const char* const write_file_messages[] = {
        [WRITE_OK] = "Write successful!",
        [WRITE_ERROR_HEADER] = "Can't write header!",
        [WRITE_ERROR_PADDING] = "Can't write padding after row in body!",
        [WRITE_ERROR_BODY] = "Can't write pixel body!"
};

const char* get_incorrect_args_message() { return incorrect_args; }

const char* get_open_file_message(enum open_status status) { return open_file_messages[status]; }

const char* get_close_file_message(enum close_status status) { return close_file_messages[status]; }

const char* get_read_file_message(enum read_status status) { return read_file_messages[status]; }

const char* get_write_file_message(enum write_status status) { return write_file_messages[status]; }
