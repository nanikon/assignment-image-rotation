//
// Created by natan on 03.01.2022.
//

#include "../include/bmp.h"

#define BMP_SIGNATURE 0x4D42
#define BIT_MAP_INFO_HEADER_SIZE 40
#define BMP_BITNESS 24

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static size_t get_padding(size_t width) {
    const size_t mod = width * sizeof(struct pixel) % 4;
    return mod ? 4 - mod : mod;
}

static enum read_status read_header( FILE* in, struct bmp_header* header) {
    const size_t result = fread(header, sizeof(struct bmp_header), 1, in);
    if (result != 1) { return READ_INVALID_HEADER; }
    if (header->bfType != BMP_SIGNATURE) { return READ_INVALID_SIGNATURE; }
    return READ_OK;
}

static enum read_status go_to_start_pixels( FILE* in, struct bmp_header header) {
    const int result = fseek(in, header.bOffBits, SEEK_SET);
    if (result != 0) { return READ_INVALID_OFFSET; }
    return READ_OK;
}

static enum read_status read_body( FILE* in, struct image* img, size_t width, size_t height) {
    for (size_t i = 0; i < height; i++) {
        struct pixel* start_row_pixel = image_row_get(img, i).value;
        const size_t pixels_read_result = fread(start_row_pixel, sizeof(struct pixel), width, in);
        if (pixels_read_result != width) { return READ_INVALID_BODY; }

        if (get_padding(width) != 0) {
            const int padding_seek_result = fseek(in, get_padding(width), SEEK_CUR);
            if (padding_seek_result != 0) { return READ_INVALID_PADDING; }
        }
    }
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header = {0};
    enum read_status header_read_result = read_header(in, &header);
    if (header_read_result != READ_OK) { return header_read_result; }

    enum read_status offset_seek_result = go_to_start_pixels(in, header);
    if (offset_seek_result != READ_OK) { return offset_seek_result; }

    image_data_full(img, header.biWidth, header.biHeight);
    enum read_status body_read_result = read_body(in, img, header.biWidth, header.biHeight);
    return body_read_result;
}

static struct bmp_header generate_header(const struct image* img) {
    size_t size_pixels_data = sizeof(struct pixel) * img->width * img->height + img->height * get_padding(img->width);
    return (struct bmp_header) {
        .bfType = BMP_SIGNATURE,
        .bfileSize = size_pixels_data + sizeof(struct bmp_header),
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BIT_MAP_INFO_HEADER_SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = 1,
        .biBitCount = BMP_BITNESS,
        .biCompression = 0,
        .biSizeImage = size_pixels_data,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

static enum write_status write_header( FILE* out, const struct image* img) {
    struct bmp_header header = generate_header(img);
    size_t result = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (result == 1) { return WRITE_OK; }
    else { return WRITE_ERROR_HEADER; }
}

static enum write_status write_padding( FILE* out, size_t width ) {
    const uint8_t zero[3] = {0};
    size_t result = fwrite(&zero, 1, get_padding(width), out);
    if (result == get_padding(width)) { return WRITE_OK; }
    else { return WRITE_ERROR_PADDING; }
}

static enum write_status write_body( FILE* out, const struct image* img) {
    for (size_t i = 0; i < img->height; i++) {
        size_t write_row_result = fwrite(&img->data[i * img->width], sizeof(struct pixel), img->width, out);
        if (write_row_result != img->width) { return WRITE_ERROR_BODY; }
        enum write_status write_padding_result = write_padding(out, img->width);
        if (write_padding_result != WRITE_OK ) { return write_padding_result; }
    }
    return WRITE_OK;
}

enum write_status to_bmp( FILE* out, struct image const* img ) {
    enum write_status header_write_result = write_header(out, img);
    if (header_write_result != WRITE_OK) { return header_write_result; }
    enum write_status body_write_result = write_body(out, img);
    return body_write_result;
}
