//
// Created by natan on 15.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_MESSAGES_H
#define ASSIGNMENT_IMAGE_ROTATION_MESSAGES_H

const char* get_incorrect_args_message();
const char* get_open_file_message(enum open_status status);
const char* get_close_file_message(enum close_status status);
const char* get_read_file_message(enum read_status status);
const char* get_write_file_message(enum write_status status);

#endif //ASSIGNMENT_IMAGE_ROTATION_MESSAGES_H
