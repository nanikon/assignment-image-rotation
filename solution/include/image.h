//
// Created by natan on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <malloc.h>
#include <stdbool.h>
#include <stdint.h>

struct __attribute__((packed)) pixel { uint8_t b, g, r; };

struct maybe_pixel {
    bool valid;
    struct pixel value;
};

struct image {
    size_t width, height;
    struct pixel* data;
};

struct maybe_image_row {
    bool valid;
    struct pixel* value;
};

// Создает image с нулевыми высотой, шириной и пустым массивом
struct image image_create();

// Вставляет в image ширину и высоту, инициализирует нужного размера data
void image_data_full(struct image* image, size_t width, size_t height);

// i - строка, j - столбец
bool image_pixel_set(struct image* image, size_t i, size_t j, struct pixel value);

struct maybe_pixel image_pixel_get(const struct image* image, size_t i, size_t j);

struct maybe_image_row image_row_get(const struct image* image, size_t i);

void image_destroy(struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
