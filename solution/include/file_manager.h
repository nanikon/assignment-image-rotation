//
// Created by natan on 04.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H

#include <stdio.h>

enum open_status {
    OPEN_READ_OK = 0,
    OPEN_WRITE_OK,
    OPEN_READ_ERROR,
    OPEN_WRITE_ERROR
};

enum open_status file_open_to_read(FILE** file, const char* file_name);

enum open_status file_open_to_write(FILE** file, const char* file_name);

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status file_close(FILE* file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_MANAGER_H
