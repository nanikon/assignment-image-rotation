//
// Created by natan on 09.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_H

#define LOG_AND_STOP_IF_NOT_OK(str, code_success, code_cur)\
        fprintf(stderr, "%s\n", str);\
        if ((code_success) != (code_cur)) { return code_cur; }

#endif //ASSIGNMENT_IMAGE_ROTATION_UTIL_H
