//
// Created by natan on 02.01.2022.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "image.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>

struct __attribute__((packed)) bmp_header;

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_HEADER,
    READ_INVALID_BODY,
    READ_INVALID_OFFSET,
    READ_INVALID_PADDING
    /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR_HEADER,
    WRITE_ERROR_PADDING,
    WRITE_ERROR_BODY
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
